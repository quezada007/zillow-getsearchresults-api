/**
 * Validate the required fields for the searchHomeForm
 *
 * @param searchForm object The searchHomeForm form with its values inputted by the user
 * @returns boolean Are the fields not empty? true or false
 */
function validateSearchForm( searchForm ){
    // is the address field empty?
    if( searchForm.address.value.trim() === '' ){
        alert('Please enter an address');
        searchForm.address.focus();
        searchForm.address.select();
        return false;
    }
    // is the citystatezip field empty?
    else if( searchForm.citystatezip.value.trim() === '' ){
        alert('Please enter a city and state or a zip code');
        searchForm.citystatezip.focus();
        searchForm.citystatezip.select();
        return false;
    }
    // both fields are not empty and it's ok to submit the form
    else{
        return true;
    }
}