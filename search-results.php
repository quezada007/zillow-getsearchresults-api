<?php
/**
 * Converts a number into US Currency
 *
 * @param $number float The number to get converted into currency
 * @return string The number formatted to US Currency. Example: 1123.45 -> $1,123.45
 */
function formatCurrency( $number ){
    // is it a negative number?
    if( $number < 0 ){
        return '-$' . abs(number_format($number));
    }
    return '$' . number_format($number);
}

// check for the address and citystatezip values from the form
if( isset($_POST['address']) && !empty($_POST['address']) && isset($_POST['citystatezip']) && !empty($_POST['citystatezip']) ){
    $address = trim($_POST['address']);
    $citystatezip = trim($_POST['citystatezip']);
    $rentzestimate = false;
    // do we need to get the Rent Zestimate Information?
    if( isset($_POST['rentzestimate']) && !empty($_POST['rentzestimate']) ){
        $rentzestimate = true;
    }

    // include the GetSearchResults class
    require_once 'zillow-api.php';
    $apiCall = new GetSearchResults($address, $citystatezip, $rentzestimate);
    $data = $apiCall->getResultsData();
    $errorCode = $apiCall->getErrorCode();
    $errorMsg = $apiCall->getErrorMessage();
} else{
    // Redirect to the Home Page
    header("Location: index.php");
    exit();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zillow GetSearchResults API Results - Jose M. Quezada</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="wrapper">
    <?php
        // if there are any errors, display the error message and show the searchHomeForm
        if( $errorCode > 0 ){
            echo '<div id="errorMsg">' . $errorMsg . '</div>';
            require_once 'search-form.php';
        }
        // if there are no errors, then display the API results
        else {
            ?>
            <div id="searchResults">
                <h1>Search Results for Zillow Property ID: <?php echo $data['zpid']; ?></h1>
                <div id="resultAdddress">
                    <h2>Address</h2>
                    <div class="result-street"><?php echo $data['address']['street']; ?></div>
                    <div class="result-city"><?php echo $data['address']['city'] . ', ' . $data['address']['state'] . ' ' . $data['address']['zipcode']; ?></div>
                    <p><strong>Latitude: </strong><?php echo $data['address']['latitude']; ?> <br/><strong>Longitude: </strong><?php echo $data['address']['longitude']; ?></p>
                    <div class="ctas">
                        <a href="<?php echo $data['links']['homedetails']; ?>" target="_blank" class="btn">Home Details</a>
                        <a href="<?php echo $data['links']['graphsanddata']; ?>" target="_blank" class="btn">Chart Data</a>
                        <a href="<?php echo $data['links']['mapthishome']; ?>" target="_blank" class="btn">Map</a>
                        <a href="<?php echo $data['links']['comparables']; ?>" target="_blank" class="btn">Similar Homes</a>
                    </div>
                </div>
                <div id="zestimate">
                    <h2>Zestimate<sup>&reg;</sup>:</h2>
                    <ul>
                        <li><strong>Zestimate:</strong> <?php echo formatCurrency($data['zestimate']['amount']); ?></li>
                        <li><strong>Last Updated:</strong> <?php echo $data['zestimate']['last-updated']; ?></li>
                        <li><strong>30-Day Change:</strong> <?php echo formatCurrency($data['zestimate']['valueChange']); ?></li>
                        <li><strong>Valuation Range High:</strong> <?php echo formatCurrency($data['zestimate']['valuationRange']['high']); ?></li>
                        <li><strong>Valuation Range Low:</strong> <?php echo formatCurrency($data['zestimate']['valuationRange']['low']); ?></li>
                        <li><strong>Percentile:</strong> <?php echo $data['zestimate']['percentile']; ?></li>
                    </ul>
                </div>
                <div id="localRealState">
                    <h2>Local Real State</h2>
                    <ul>
                        <li><strong>Region:</strong> <?php echo $data['localRealEstate']['region']['@attributes']['name']; ?></li>
                        <li><strong>ID:</strong> <?php echo $data['localRealEstate']['region']['@attributes']['id']; ?></li>
                        <li><strong>Type:</strong> <?php echo $data['localRealEstate']['region']['@attributes']['type']; ?></li>
                        <?php
                            // if the Zillow Home Value Index is available, then display it
                            if( !empty($data['localRealEstate']['region']['zindexValue']) ){
                                echo '<li><strong>Zillow Home Value Index:</strong> ' . $data['localRealEstate']['region']['zindexValue'] . '</li>';
                            }
                            // if the Zillow Home Value Index 1-Yr change is available, then display it
                            if( !empty($data['localRealEstate']['region']['zindexOneYearChange']) ){
                                echo '<li><strong>Zillow Home Value Index 1-Year Change:</strong> ' . $data['localRealEstate']['region']['zindexOneYearChange'] . '</li>';
                            }
                        ?>
                    </ul>
                    <div class="ctas">
                        <a href="<?php echo $data['localRealEstate']['region']['links']['overview']; ?>" target="_blank" class="btn">Region Overview Page</a>
                        <a href="<?php echo $data['localRealEstate']['region']['links']['forSaleByOwner']; ?>" target="_blank" class="btn">Sale by Owner Page</a>
                        <a href="<?php echo $data['localRealEstate']['region']['links']['forSale']; ?>" target="_blank" class="btn">Sale Homes Page</a>
                    </div>
                </div>
                <?php
                    // Display the Rent Zestimate Information if we have it
                    if( $rentzestimate ) {
                        ?>
                        <div id="rentzestimate">
                            <h2>Rent Zestimate<sup>&reg;</sup>:</h2>
                            <ul>
                                <li><strong>Rent Zestimate:</strong> <?php echo formatCurrency($data['rentzestimate']['amount']); ?></li>
                                <li><strong>Last Updated:</strong> <?php echo $data['rentzestimate']['last-updated']; ?></li>
                                <li><strong>30-Day Change:</strong> <?php echo formatCurrency($data['rentzestimate']['valueChange']); ?></li>
                                <li><strong>Valuation Range High:</strong> <?php echo formatCurrency($data['rentzestimate']['valuationRange']['high']); ?></li>
                                <li><strong>Valuation Range Low:</strong> <?php echo formatCurrency($data['rentzestimate']['valuationRange']['low']); ?></li>
                            </ul>
                        </div>
                        <?php
                    }
                ?>

            </div>
            <?php
            // display the searhHomeForm at the bottom of the page so the user can make another search
            require_once 'search-form.php';
        } // End of else statement
    ?>
</div>
<script src="js/search.js"></script>
</body>
</html>