<div id="searchHome">
    <h1>Find Your Home!</h1>
    <form id="searchHomeForm" action="search-results.php" method="post" onsubmit="return validateSearchForm(this)">
        <div class="input-field">
            <label for="address">Address: </label>
            <input type="text" id="address" name="address" placeholder="Enter an address" required>
        </div>
        <div class="input-field">
            <label for="citystatezip">City &amp; State or Zip Code: </label>
            <input type="text" id="citystatezip" name="citystatezip" placeholder="Enter a city and state or a zip code" required>
        </div>
        <div class="input-field">
            <label class="checkbox"><input type="checkbox" name="rentzestimate" value="yes"> Include Rent Zestimate Information</label>
        </div>
        <div class="input-field">
            <button type="submit" class="btn">Search</button>
        </div>
    </form>
</div>