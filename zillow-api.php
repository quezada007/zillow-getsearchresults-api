<?php

/**
 * Class GetSearchResults
 *
 * Finds a property for a specified address using the GetSearchResults API from Zillow's API Network
 *
 * @author Jose M. Quezada <quezada007@yahoo.com>
 */
class GetSearchResults {

    /**
     * @var string The URL for the GetSearchResults API Web Service
     */
    private $url = 'http://www.zillow.com/webservice/GetSearchResults.htm';

    /**
     * @var string The Zillow Web Service Identifier. The parameter is "zws-id" (required)
     */
    private $apiKey = 'X1-ZWz1dyb53fdhjf_6jziz';

    /**
     * @var string The address of the property to search. This string should be URL encoded. The parameter is "address" (required)
     */
    private $address;

    /**
     * @var string The city+state combination and/or ZIP code for which to search. This string should be URL encoded.
     * Note that giving both city and state is required. Using just one will not work. The parameter is "citystatezip" (required)
     */
    private $citystatezip;

    /**
     * @var boolean If set to "true", it returns the Rent Zestimate information. The default is false. The parameter is "rentzestimate" (Not required)
     */
    private $rentzestimate;

    /**
     * @var array The data returned from the API
     */
    private $data;

    /**
     * @var string The error message returned from the API
     */
    private $errorMsg = '';

    /**
     * @var integer The error code returned from the API
     */
    private $errorCode;

    /**
     * The GetSearchResults constructor
     *
     * @param $address string The address of the property to search
     * @param $citystatezip string The city and state combination and/or ZIP code for which to search
     * @param $rentzestimate boolean Would you like Rent Zestimate Information? true or false
     */
    public function __construct($address, $citystatezip, $rentzestimate){
        $this->address = urlencode($address);
        $this->citystatezip = urlencode($citystatezip);
        $this->rentzestimate = $rentzestimate;
        // find the property
        $this->findProperty();
    }

    /**
     * Creates the URL needed for the API call
     *
     * @return string The URL needed to make the API call
     */
    private function getUrl(){
        $url = $this->url . '?zws-id=' . $this->apiKey . '&address=' . $this->address . '&citystatezip=' . $this->citystatezip;
        // do we need to get the Rent Zestimate information?
        if( $this->rentzestimate ){
            $url .= '&rentzestimate=true';
        }
        return $url;
    }

    /**
     * Return an array with all the options needed for the cURL
     *
     * @return array The cURL options needed for fetching the Zillow API data
     */
    private function getOptionsArray(){
        return array(
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        );
    }

    /**
     * Get the xml data from Zillow's API
     *
     * @return bool|mixed The xml data returned from  or false when it failed to get the data
     */
    private function fetchData(){
        $url = $this->getUrl();
        $ch = curl_init( $url );
        curl_setopt_array( $ch, $this->getOptionsArray() );
        $curlResponse = curl_exec( $ch );
        // Check for success
        if( !$curlResponse ){
            $this->errorMsg .= 'We did not get a response. cURL Error: ' . curl_error ( $ch );
            $this->errorCode = 1000;
        }
        curl_close($ch);
        $xml = simplexml_load_string( $curlResponse );
        // convert the XML into an array
        $this->data = json_decode(json_encode($xml),true);
    }

    /**
     * Set the error message and the error code returned from the API call
     */
    private function setErrors(){
        $this->errorMsg .= $this->data['message']['text'];
        $this->errorCode = (int)$this->data['message']['code'];
    }

    /**
     * Fetch all the data from the API and store all the error messages
     */
    private function findProperty(){
        // get the data from the API
        $this->fetchData();
        // get any error messages and error codes
        $this->setErrors();
    }

    /**
     * Get all the data returned from the API call
     *
     * @return array Returns all the data from the API
     */
    public function getResultsData(){
        // if we have results, then return that
        if( !empty($this->data['response']['results']['result']) ) {
            return $this->data['response']['results']['result'];
        }
        return $this->data;
    }

    /**
     * @return string Returns any error messages
     */
    public function getErrorMessage(){
        return $this->errorMsg;
    }

    /**
     * @return integer Returns the error code returned from the API call
     */
    public function getErrorCode(){
        return $this->errorCode;
    }
}
